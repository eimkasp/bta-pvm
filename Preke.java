package com.bta;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Preke {

    static int PVM = 21;

    private BigDecimal kaina; // be pvm

    private BigDecimal pvmSuma;

    private String pavadinimas;

    private int kiekis = 0;




    public int getKiekis() {
        return kiekis;
    }

    public void setKiekis(int kiekis) {
        this.kiekis = kiekis;
    }

    Preke(BigDecimal kaina, String pavadinimas) {
        this.kaina = kaina;
        this.pavadinimas = pavadinimas;
        this.kiekis = 1;
        this.skaiciuotiPVMSuma(2);
    }

    public void spausdinti(int tikslumas) {
        System.out.println("Preke: " + this.pavadinimas);
        System.out.println("Kiekis: " + this.kiekis);
        System.out.println("Prekes PVM suma: " + this.pvmSuma);
        System.out.println("Prekes kaina be PVM: " + this.kaina);
        System.out.println("Prekes kaina su PVM: " + this.kaina.add(this.pvmSuma));
        System.out.println("Bendra kaina su PVM: " + this.kaina.add(this.pvmSuma).multiply(new BigDecimal(this.kiekis)));
        System.out.println("Bendra kaina be PVM: " + this.kaina.multiply(new BigDecimal(this.kiekis)));

    }

    public void skaiciuotiPVMSuma(int tikslumas) {
        this.pvmSuma =  this.kaina.divide(new BigDecimal(100)).multiply(new BigDecimal(Preke.PVM)).setScale(tikslumas, RoundingMode.HALF_UP);
    }

    public double sumaSuPVM() {
        double test = this.kaina.add(this.pvmSuma).doubleValue();
        System.out.println(test);
        return test;
    }



    public void pridetiPreke() {
        this.kiekis++;
    }

    public String getPavadinimas() {
        return pavadinimas;
    }

    public void setPavadinimas(String pavadinimas) {
        this.pavadinimas = pavadinimas;
    }


    public BigDecimal getKaina() {
        return kaina;
    }

    public void setKaina(BigDecimal kaina) {
        this.kaina = kaina;
    }



}
